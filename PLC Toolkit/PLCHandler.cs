﻿using S7.Net;

namespace NS.PLCToolkit
{
    public class PLCHandler
    {
        public Plc PlcObject { get; set; }

        public PLCHandler(Plc plc)
        {
            PlcObject = plc;
        }

        public bool OpenAndTestConnection()
        {
            PlcObject.Open();

            bool isSocketOpen = PlcObject.IsAvailable;
            bool isConnectionAvailable = PlcObject.IsConnected;

            return isSocketOpen && isConnectionAvailable;
        }

        public bool DisconnectAndTestConnection(Plc plc)
        {
            plc.Close();

            return plc.IsAvailable;
        }
    }
}
