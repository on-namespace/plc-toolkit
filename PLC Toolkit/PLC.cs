﻿using System;
using S7.Net;

namespace NS.PLCToolkit
{
    public class PLC
    {
        public string IP { get; set; }
        public PLCType PLCType { get; set; }
        public short Rack { get; set; }
        public short Slot { get; set; }

        private Plc PlcObject = null;
        public PLCHandler PLCHandler { get; set; }

        public PLC(string ip, PLCType type, short rack, short slot)
        {
            IP = ip;
            PLCType = type;
            Rack = rack;
            Slot = slot;

            bool isAvailable = false;

            switch (PLCType)
            {
                case PLCType.SIEMENS_S7_1200:
                    PlcObject = new Plc(CpuType.S71200, ip, rack, slot);
                    PLCType = PLCType.SIEMENS_S7_1200;
                    isAvailable = CreateConnection(PlcObject);
                    break;
                default:
                    break;
            }

            if (isAvailable == false)
            {
                throw new Exception("Did not Open Connection to the PLC");
            }
        }

        private bool CreateConnection(Plc plc)
        {
            if (PlcObject == null) { return false; }

            PLCHandler = new PLCHandler(plc);
            return PLCHandler.OpenAndTestConnection();
        }
    }
}
